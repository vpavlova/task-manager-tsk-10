package ru.vpavlova.tm.api;

public interface IProjectController {

    void showProjectList();

    void createProject();

    void clearProject();

}
